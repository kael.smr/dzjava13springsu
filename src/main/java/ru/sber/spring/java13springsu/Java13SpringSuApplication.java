package ru.sber.spring.java13springsu;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java13SpringSuApplication implements CommandLineRunner {



    public static void main(String[] args) {
        SpringApplication.run(Java13SpringSuApplication.class, args);
    }


    @Override
    public void run(String... args){
    }
}
