package ru.sber.spring.java13springsu.filmography.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.spring.java13springsu.filmography.constaints.UserRoleConstants;
import ru.sber.spring.java13springsu.filmography.dto.BuyerDTO;
import ru.sber.spring.java13springsu.filmography.service.BuyerService;

@Controller
@Hidden
@RequestMapping("/buyers")
public class MVCBuyerContoroller implements UserRoleConstants {
    private final BuyerService buyerService;
    public MVCBuyerContoroller(BuyerService buyerService){
        this.buyerService = buyerService;
    }
    @GetMapping("/registration")
    public String registration(Model model){
        model.addAttribute("buyerForm", new BuyerDTO());
        return "registration";
    }
    @PostMapping("/registration")
    public String registration(@ModelAttribute("buyerForm") BuyerDTO buyerDTO, BindingResult bindingResult){


        if (buyerDTO.getLogin().equalsIgnoreCase(ADMIN) || buyerService.getBuyerByLogin(buyerDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "The login is already occupied");
            return "registration";
        }
        if (buyerService.getBuyerByEmail(buyerDTO.getEmail()) != null){
            bindingResult.rejectValue("email", "error.email", "The email is already occupied");
            return "registration";
        }
        buyerService.create(buyerDTO);
        return "redirect:login";
    }
}
