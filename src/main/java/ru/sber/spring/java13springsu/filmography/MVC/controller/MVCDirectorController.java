package ru.sber.spring.java13springsu.filmography.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.spring.java13springsu.filmography.dto.DirectorDTO;
import ru.sber.spring.java13springsu.filmography.dto.DirectorsWithFilmsDTO;
import ru.sber.spring.java13springsu.filmography.service.DirectorService;

import java.util.List;

@Controller
@RequestMapping("directors")
public class MVCDirectorController {
    private final DirectorService directorService;

    public MVCDirectorController(DirectorService directorService){
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model){
        List<DirectorsWithFilmsDTO> directorsDTOList = directorService.getAllDirectorsWithFilms();
        model.addAttribute("directors",directorsDTOList);
        return "directors/viewAllDirectors";
    }
    @GetMapping("/add")
    public String create(){
        return "directors/addDirector";
    }
    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm")DirectorDTO directorDTO){
        directorService.create(directorDTO);
        return "redirect:/directors";
    }
}
