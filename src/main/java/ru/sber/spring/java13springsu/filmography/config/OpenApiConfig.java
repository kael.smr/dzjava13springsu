package ru.sber.spring.java13springsu.filmography.config;


import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI filmographyProject(){
        return new OpenAPI()
                .info(new Info().title("Online Film Library")
                        .description("A service that allows you to rent a movie in an online movie library")
                        .version("v0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                        .contact(new Contact().name("Maxim E.P.")
                                .email("max163@inbox.ru")
                                .url(""))
                        );
    }
}
