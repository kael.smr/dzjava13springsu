package ru.sber.spring.java13springsu.filmography.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.sber.spring.java13springsu.filmography.constaints.UserRoleConstants;
import ru.sber.spring.java13springsu.filmography.service.buyersdetails.CustomBuyersDetailsService;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig implements UserRoleConstants {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CustomBuyersDetailsService customBuyerDetailsService;
    private final List<String> RESOURCES_WHITE_LIST = List.of("/resources",
            "/js/**",
            "/swagger-ui",
            "/");
    private final List<String> FILMS_WHITE_LIST = List.of("/films");
    private final List<String> FILMS_PERMISSION_LIST = List.of("/films/add",
            "/films/update",
            "films/delete");
    private final List<String> BUYERS_WHITE_LIST = List.of("/login",
            "/buyers/registration",
            "/buyers/remember-password",
            "/buyers/change-password");

    public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
                             CustomBuyersDetailsService customBuyersDetailsService
                             ) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.customBuyerDetailsService = customBuyersDetailsService;
    }
    @Bean
    public HttpFirewall httpFirewall(){
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedPercent(true);
        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowSemicolon(true);
        firewall.setAllowedHttpMethods(Arrays.asList("GET","POST","PUT","DELETE"));
        return firewall;
    }
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception{
        http
//                csrf().disable()
                .csrf(csrf -> csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                )
                //настраиваем http запросы - кому куда можно/нельзя
                .authorizeHttpRequests((request -> request
                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(FILMS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(BUYERS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(FILMS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN,EMPLOYEE)
                        .anyRequest().authenticated())
                )
                //настройка для входа в систему
                .formLogin((form -> form.loginPage("/login")
                        //перенаправление на изначальную страницу в случае успешной авторизации
                        .defaultSuccessUrl("/")
                        .permitAll()
                ))
                .logout(logout -> logout.logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID")
                        .permitAll()
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                );
        return http.build();
    }
    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{
        authenticationManagerBuilder.userDetailsService(customBuyerDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
