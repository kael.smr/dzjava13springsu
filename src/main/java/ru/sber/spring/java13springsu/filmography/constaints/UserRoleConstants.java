package ru.sber.spring.java13springsu.filmography.constaints;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String EMPLOYEE = "EMPLOYEE";
    String BUYER = "BUYER";
}
