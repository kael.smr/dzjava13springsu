package ru.sber.spring.java13springsu.filmography.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.filmography.dto.BuyerDTO;
import ru.sber.spring.java13springsu.filmography.model.Buyer;
import ru.sber.spring.java13springsu.filmography.service.BuyerService;

@RestController
@RequestMapping(value = "/buyers")
@Tag(name = "Users",
description = "Controller for working with movie users")
public class BuyerController extends GenericController<Buyer, BuyerDTO> {


    public BuyerController(BuyerService buyerService){
        super(buyerService);
    }
}
