package ru.sber.spring.java13springsu.filmography.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.filmography.dto.DirectorDTO;
import ru.sber.spring.java13springsu.filmography.model.Director;
import ru.sber.spring.java13springsu.filmography.service.DirectorService;


@RestController
@RequestMapping(value = "/directors")
//http://localhost:9090/books
@Tag(name = "Director",
        description = "Controller for working with film directors")
public class DirectorController extends GenericController<Director, DirectorDTO> {




    public DirectorController(DirectorService directorService){
        super(directorService);

    }

    @Operation(description = "Add a movie to the director", method = "addFilm")
    @RequestMapping(value = "addFilm",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void addDirector(long directorId, long filmId){
        DirectorDTO object = service.getOne(directorId);
        object.getFilmIds().add(filmId);
        service.repository.save(service.mapper.toEntity(object));
    }





}
