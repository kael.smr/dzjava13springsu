package ru.sber.spring.java13springsu.filmography.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.filmography.dto.FilmDTO;
import ru.sber.spring.java13springsu.filmography.model.Film;
import ru.sber.spring.java13springsu.filmography.service.FilmService;


@RestController
@RequestMapping(value = "/films")
//http://localhost:9090/books
@Tag(name = "Film",
description = "Controller for working with films of the film library")
public class FilmController extends GenericController<Film, FilmDTO> {



    protected FilmController(FilmService filmService){
        super(filmService);

    }
    @Operation(description = "Add a director to a movie", method = "addDirector")
    @RequestMapping(value = "addDirector",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void addDirector(long directorId, long filmId){
        FilmDTO object = service.getOne(filmId);
        object.getDirectorIds().add(directorId);
        service.repository.save(service.mapper.toEntity(object));
    }



//http://localhost:9090/api/rest/swagger-ui/index.html#/ - подключение swagger'а
//http://localhost:9009/api/rest/books/getById?id=1


}
