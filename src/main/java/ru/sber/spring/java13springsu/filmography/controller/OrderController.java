package ru.sber.spring.java13springsu.filmography.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springsu.filmography.dto.OrderDTO;
import ru.sber.spring.java13springsu.filmography.model.Order;
import ru.sber.spring.java13springsu.filmography.service.OrderService;

@RestController
@RequestMapping(name = "/orders")
@Tag(name = "Order",
description = "Controller for working with movie orders")
public class OrderController extends GenericController<Order, OrderDTO>{

    public OrderController(OrderService orderService){
        super(orderService);
    }

    @Operation(description = "Add order", method = "addOrder")
    @RequestMapping(value = "addOrder",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void addOrder(long filmId, long buyerId){
        OrderDTO order = new OrderDTO();
        service.create(order).setBuyerId(buyerId);
        service.create(order).setFilmId(filmId);
        service.repository.save(service.mapper.toEntity(order));


    }
}
