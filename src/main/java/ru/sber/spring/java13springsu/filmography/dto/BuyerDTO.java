package ru.sber.spring.java13springsu.filmography.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BuyerDTO extends GenericDTO {
    private String login;
    private String password;
    private String lastName;
    private String firstName;
    private String middleName;
    private String birthDate;
    private String phone;
    private String address;
    private String email;
    private Set<Long> orderIds;
    private RoleFilmDTO role;
    private Set<Long> buyerOrders;

}
