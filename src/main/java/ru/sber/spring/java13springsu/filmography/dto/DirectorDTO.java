package ru.sber.spring.java13springsu.filmography.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DirectorDTO extends GenericDTO{
    private List<DirectorDTO> directorDTOS;
    private String directorsFio;
    private String position;
    private Set<Long> filmIds;
}
