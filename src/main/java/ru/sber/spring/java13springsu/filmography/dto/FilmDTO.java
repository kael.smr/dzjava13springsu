package ru.sber.spring.java13springsu.filmography.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.java13springsu.filmography.model.Genre;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor

public class FilmDTO extends GenericDTO{
    private List<FilmDTO> filmDTOS;
    private String title;
    private String premierYear;
    private String country;
    private Genre genre;
    private Set<Long> directorIds;
}
