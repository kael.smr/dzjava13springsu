package ru.sber.spring.java13springsu.filmography.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmsWithDirectorsDTO extends FilmDTO{
    private Set<DirectorDTO> directors;
}
