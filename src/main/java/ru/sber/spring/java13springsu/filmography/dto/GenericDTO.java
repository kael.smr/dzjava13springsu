package ru.sber.spring.java13springsu.filmography.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@NoArgsConstructor
public class GenericDTO {

    private Long id;
    private String createdBy;
    private LocalDateTime createdWhen;


}
