package ru.sber.spring.java13springsu.filmography.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleFilmDTO {
    private Long id;
    private String title;
    private String description;
    private Set<Long> buyersIds;
}
