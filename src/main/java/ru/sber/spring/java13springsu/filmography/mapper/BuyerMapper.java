package ru.sber.spring.java13springsu.filmography.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.filmography.dto.BuyerDTO;
import ru.sber.spring.java13springsu.filmography.model.Buyer;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;
import ru.sber.spring.java13springsu.filmography.repository.OrderRepository;
import ru.sber.spring.java13springsu.filmography.utils.DateFormatter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BuyerMapper extends GenericMapper<Buyer, BuyerDTO> {

    private OrderRepository orderRepository;
    protected BuyerMapper(ModelMapper modelMapper, OrderRepository orderRepository) {
        super(modelMapper, Buyer.class, BuyerDTO.class);
        this.orderRepository = orderRepository;
    }
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Buyer.class, BuyerDTO.class)
                .addMappings(m -> m.skip(BuyerDTO::setBuyerOrders)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(BuyerDTO.class,Buyer.class)
                .addMappings(m -> m.skip(Buyer::setOrders)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Buyer::setBirthDate)).setPostConverter(toEntityConverter());
    }


    @Override
    protected void mapSpecificFields(BuyerDTO source, Buyer destination) {
        if (!Objects.isNull(source.getBuyerOrders())){
            destination.setOrders(new HashSet<>(orderRepository.findAllById(source.getBuyerOrders())));
        } else {
            destination.setOrders(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));
    }


    @Override
    protected void mapSpecificFields(Buyer source, BuyerDTO destination) {
        destination.setBuyerOrders(getIds(source));
    }


    @Override
    protected Set<Long> getIds(Buyer entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getOrders())
                ? null
                : entity.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
