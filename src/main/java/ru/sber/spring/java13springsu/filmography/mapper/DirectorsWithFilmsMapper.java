package ru.sber.spring.java13springsu.filmography.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.filmography.dto.DirectorsWithFilmsDTO;
import ru.sber.spring.java13springsu.filmography.model.Director;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;
import ru.sber.spring.java13springsu.filmography.repository.FilmRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorsWithFilmsMapper extends GenericMapper<Director, DirectorsWithFilmsDTO> {

    private final FilmRepository filmRepository;

    protected DirectorsWithFilmsMapper(ModelMapper modelMapper, FilmRepository filmRepository) {
        super(modelMapper, Director.class, DirectorsWithFilmsDTO.class);
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper(){
        modelMapper.createTypeMap(Director.class, DirectorsWithFilmsDTO.class)
                .addMappings(m->m.skip(DirectorsWithFilmsDTO::setFilmIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DirectorsWithFilmsDTO.class,Director.class)
                .addMappings(m->m.skip(Director::setFilms)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(DirectorsWithFilmsDTO source, Director destination) {
        destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmIds())));
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorsWithFilmsDTO destination) {
        destination.setFilmIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Director entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                :entity.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
