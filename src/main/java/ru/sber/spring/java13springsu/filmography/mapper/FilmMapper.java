package ru.sber.spring.java13springsu.filmography.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.filmography.dto.FilmDTO;
import ru.sber.spring.java13springsu.filmography.model.Film;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;
import ru.sber.spring.java13springsu.filmography.repository.DirectorRepository;
import ru.sber.spring.java13springsu.filmography.utils.DateFormatter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {

    private final DirectorRepository repository;
    protected FilmMapper(ModelMapper modelMapper, DirectorRepository repository) {
        super(modelMapper, Film.class, FilmDTO.class);
        this.repository = repository;
    }

    @PostConstruct
    public void setupMapper(){
        modelMapper.createTypeMap(Film.class,FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class,Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter())
                .addMappings(m->m.skip(Film::setPremierYear)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getDirectorIds()))
            destination.setDirectors(new HashSet<>(repository.findAllById(source.getDirectorIds())));
        else {
            destination.setDirectors(Collections.emptySet());
        }
        destination.setPremierYear(DateFormatter.formatStringToDate(source.getPremierYear()));

    }
    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorIds(getIds(source));
    }
    @Override
    protected Set<Long> getIds(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getDirectors())
                ? null
                :entity.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
