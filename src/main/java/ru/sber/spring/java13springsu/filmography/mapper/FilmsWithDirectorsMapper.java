package ru.sber.spring.java13springsu.filmography.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.sber.spring.java13springsu.filmography.dto.FilmsWithDirectorsDTO;
import ru.sber.spring.java13springsu.filmography.model.Film;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;
import ru.sber.spring.java13springsu.filmography.repository.DirectorRepository;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
@Component
public class FilmsWithDirectorsMapper extends GenericMapper<Film, FilmsWithDirectorsDTO>{

    private final DirectorRepository directorRepository;
    protected FilmsWithDirectorsMapper(ModelMapper modelMapper, DirectorRepository directorRepository) {
        super(modelMapper, Film.class, FilmsWithDirectorsDTO.class);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    protected void setupMapper(){
        modelMapper.createTypeMap(Film.class, FilmsWithDirectorsDTO.class)
                .addMappings(m->m.skip(FilmsWithDirectorsDTO::setDirectorIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmsWithDirectorsDTO.class,Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmsWithDirectorsDTO source, Film destination) {
        destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorIds())));
    }

    @Override
    protected void mapSpecificFields(Film source, FilmsWithDirectorsDTO destination) {
        destination.setDirectorIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Film entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                :entity.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
