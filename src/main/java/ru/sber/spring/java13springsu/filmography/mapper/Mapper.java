package ru.sber.spring.java13springsu.filmography.mapper;

import ru.sber.spring.java13springsu.filmography.dto.GenericDTO;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;

import java.util.List;

public interface Mapper <E extends GenericModel, D extends GenericDTO>{
    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtos);

    List<D> toDTOs(List<E> entities);
}
