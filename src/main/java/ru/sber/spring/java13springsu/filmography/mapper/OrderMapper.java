package ru.sber.spring.java13springsu.filmography.mapper;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springsu.filmography.dto.OrderDTO;
import ru.sber.spring.java13springsu.filmography.model.Order;
import ru.sber.spring.java13springsu.filmography.repository.BuyerRepository;
import ru.sber.spring.java13springsu.filmography.repository.DirectorRepository;
import ru.sber.spring.java13springsu.filmography.repository.FilmRepository;

import java.util.Set;
@Component
public class OrderMapper extends GenericMapper<Order, OrderDTO> {

    private final FilmRepository filmRepository;
    private final BuyerRepository buyerRepository;

    protected OrderMapper(ModelMapper modelMapper, FilmRepository filmRepository, DirectorRepository directorRepository, BuyerRepository buyerRepository) {
        super(modelMapper, Order.class,OrderDTO.class);
        this.filmRepository = filmRepository;
        this.buyerRepository = buyerRepository;
    }

    @PostConstruct
    public void setupMapper(){
        super.modelMapper.createTypeMap(Order.class,OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setBuyerId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() ->new NotFoundException("Фильм не найден")));
        destination.setBuyer(buyerRepository.findById(source.getBuyerId()).orElseThrow(() ->new NotFoundException("Покупатель не найден")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setBuyerId(source.getBuyer().getId());
        destination.setFilmId(source.getFilm().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод не доступен");
    }
}
