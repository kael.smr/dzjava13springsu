package ru.sber.spring.java13springsu.filmography.model;

public enum Genre {

    FANTASY("Фантастика"),
    DRAMA("Драма"),
    HORROR("Ужасы"),
    ACTION_MOVIE("Боевик"),
    COMEDY("Комедия");

    private final String genreDisplayValue;

    Genre(String text){
        this.genreDisplayValue = text;
    }
    public String getGenreDisplayValue(){
        return this.genreDisplayValue;
    }
}
