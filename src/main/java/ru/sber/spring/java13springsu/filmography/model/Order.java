package ru.sber.spring.java13springsu.filmography.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "orders_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Order extends GenericModel{

    @ManyToOne
    @JoinColumn(name = "buyer_id", foreignKey = @ForeignKey(name = "FK_BUYER_ORDERS"))
    private Buyer buyer;

    @ManyToOne
    @JoinColumn(name = "films_id",foreignKey = @ForeignKey(name = "FK_FILM_ORDERS"))
    private Film film;

    //полу автоматически должно рассчитываться из rent_date + rent_period
    @Column(name = "retern_date")
    private LocalDateTime rentDate;

    //rent_period - количество дней аренды, если не указано, то по-умолчанию - 14 дней.
    @Column(name = "rent_period")
    private Integer rentPeriod;

    //если purchase - true, то rent_date и rent_period = NULL
    @Column(name = "purchase")
    private Boolean purchase;



}
