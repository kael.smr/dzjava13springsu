package ru.sber.spring.java13springsu.filmography.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.filmography.model.Buyer;

@Repository

public interface BuyerRepository extends GenericRepository<Buyer> {
    Buyer findBuyerByLogin(String login);
    Buyer findBuyerByEmail(String email);
}
