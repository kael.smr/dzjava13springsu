package ru.sber.spring.java13springsu.filmography.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.filmography.model.Director;

@Repository

public interface DirectorRepository extends GenericRepository<Director> {
}
