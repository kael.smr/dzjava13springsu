package ru.sber.spring.java13springsu.filmography.repository;


import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springsu.filmography.model.Film;
@Repository

public interface FilmRepository extends GenericRepository<Film> {
}
