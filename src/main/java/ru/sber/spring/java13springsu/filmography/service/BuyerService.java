package ru.sber.spring.java13springsu.filmography.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.filmography.dto.BuyerDTO;
import ru.sber.spring.java13springsu.filmography.dto.RoleFilmDTO;
import ru.sber.spring.java13springsu.filmography.mapper.BuyerMapper;
import ru.sber.spring.java13springsu.filmography.model.Buyer;
import ru.sber.spring.java13springsu.filmography.repository.BuyerRepository;

@Service
public class BuyerService extends GenericService<Buyer, BuyerDTO> {



    protected BuyerService(BuyerRepository buyerRepository, BuyerMapper buyerMapper) {
        super(buyerRepository, buyerMapper);
    }

    @Override
    public BuyerDTO create(BuyerDTO object) {
        RoleFilmDTO roleFilmDTO = new RoleFilmDTO();
        roleFilmDTO.setId(1L);
        object.setRole(roleFilmDTO);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
}
