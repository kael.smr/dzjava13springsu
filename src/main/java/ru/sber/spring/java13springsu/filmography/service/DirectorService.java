package ru.sber.spring.java13springsu.filmography.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.filmography.dto.DirectorDTO;
import ru.sber.spring.java13springsu.filmography.dto.DirectorsWithFilmsDTO;
import ru.sber.spring.java13springsu.filmography.mapper.DirectorMapper;
import ru.sber.spring.java13springsu.filmography.mapper.DirectorsWithFilmsMapper;
import ru.sber.spring.java13springsu.filmography.model.Director;
import ru.sber.spring.java13springsu.filmography.repository.DirectorRepository;

import java.util.List;

@Service
public class DirectorService extends GenericService<Director, DirectorDTO> {

    private final DirectorsWithFilmsMapper directorsWithFilmsMapper;
    private final DirectorRepository directorRepository;
    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper mapper,
                              DirectorsWithFilmsMapper directorsWithFilmsMapper){
        super(directorRepository, mapper);
        this.directorsWithFilmsMapper = directorsWithFilmsMapper;
        this.directorRepository = directorRepository;
    }

    public List<DirectorsWithFilmsDTO> getAllDirectorsWithFilms(){
        return directorsWithFilmsMapper.toDTOs(directorRepository.findAll());
    }

    public void addFilm(long filmIds, long directorIds) {
        DirectorDTO object = getOne(directorIds);
        object.getFilmIds().add(filmIds);
        repository.save(mapper.toEntity(object));
    }

    public void addFilm(long filmIds, DirectorDTO object){
        object.getFilmIds().add(filmIds);
        repository.save(mapper.toEntity(object));
    }





}
