package ru.sber.spring.java13springsu.filmography.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.filmography.dto.FilmDTO;
import ru.sber.spring.java13springsu.filmography.dto.FilmsWithDirectorsDTO;
import ru.sber.spring.java13springsu.filmography.mapper.FilmMapper;
import ru.sber.spring.java13springsu.filmography.mapper.FilmsWithDirectorsMapper;
import ru.sber.spring.java13springsu.filmography.model.Film;
import ru.sber.spring.java13springsu.filmography.repository.FilmRepository;

import java.util.List;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {

    private final FilmsWithDirectorsMapper filmsWithDirectorsMapper;
    private final FilmRepository filmRepository;
    protected FilmService (FilmRepository filmRepository,
                           FilmMapper mapper,
                           FilmsWithDirectorsMapper filmsWithDirectorsMapper) {
        super(filmRepository, mapper);
        this.filmsWithDirectorsMapper = filmsWithDirectorsMapper;
        this.filmRepository = filmRepository;
    }
    public List<FilmsWithDirectorsDTO> getAllFilmsWithDirectors(){
        return filmsWithDirectorsMapper.toDTOs(filmRepository.findAll());
    }





}
