package ru.sber.spring.java13springsu.filmography.service;

import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springsu.filmography.dto.GenericDTO;
import ru.sber.spring.java13springsu.filmography.mapper.GenericMapper;
import ru.sber.spring.java13springsu.filmography.model.GenericModel;
import ru.sber.spring.java13springsu.filmography.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;

/*
 * Абстрактный сервис который хранит в себе реализацию CRUD операций по умолчанию
 * Если реализация отличная от того что представлено в этом классе,
 * то она переопределяется в сервисе для конкретной сущности
 * @param <T> - сущность с которой мы работаем
 * @param <N> - DTO, которую мы будем отдавать/принимать
 */
@Service
public abstract class GenericService <T extends GenericModel, N extends GenericDTO> {
    public final GenericRepository<T> repository;

    public final GenericMapper<T,N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService( GenericRepository<T> repository,GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }



    public List<N> listAll(){
        return mapper.toDTOs(repository.findAll());
    }
    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " нет")));
    }

    public N create(N newObject){
        //TODO: переделать, когда будет Spring Security
        newObject.setCreatedBy("Admin");
        newObject.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public N update(N updateObject){
        return mapper.toDTO(repository.save(mapper.toEntity(updateObject)));
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
