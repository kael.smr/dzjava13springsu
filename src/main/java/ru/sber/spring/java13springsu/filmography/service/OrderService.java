package ru.sber.spring.java13springsu.filmography.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.filmography.dto.OrderDTO;
import ru.sber.spring.java13springsu.filmography.mapper.OrderMapper;
import ru.sber.spring.java13springsu.filmography.model.Order;
import ru.sber.spring.java13springsu.filmography.repository.OrderRepository;

@Service
public class OrderService extends GenericService <Order, OrderDTO> {
    protected OrderService(OrderRepository repository, OrderMapper mapper) {
        super(repository, mapper);
    }
}



