package ru.sber.spring.java13springsu.filmography.service.buyersdetails;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.sber.spring.java13springsu.filmography.constaints.UserRoleConstants;
import ru.sber.spring.java13springsu.filmography.model.Buyer;
import ru.sber.spring.java13springsu.filmography.repository.BuyerRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomBuyersDetailsService implements UserDetailsService {
    @Value("${spring.security.user.name}")
    private String adminUserName;
    @Value("${spring.security.user.password}")
    private String adminPassword;
    @Value("${spring.security.user.roles}")
    private String adminRole;


    private final BuyerRepository buyerRepository;
    public CustomBuyersDetailsService(BuyerRepository buyerRepository){
        this.buyerRepository = buyerRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.equals(adminUserName)) {
            return new CustomBuyersDetails(null, username, adminPassword, List.of(new SimpleGrantedAuthority("ROLE_" + adminRole)));
        } else {
            Buyer buyer = buyerRepository.findBuyerByLogin(username);
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(buyer.getRole().getId() == 1L ? "ROLE_" + UserRoleConstants.BUYER :
            "ROLE_" + UserRoleConstants.EMPLOYEE));
            return new CustomBuyersDetails(buyer.getId().intValue(), username, buyer.getPassword(), authorities);
        }
    }
}
